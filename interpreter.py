from collections import namedtuple
import sys

from colorama import Back,Style,Fore
from math import exp,log
DIRS = [(-1,0),(0,-1),(1,0),(0,1)]
formirror  = [3,2,1,0]
backmirror = [1,0,3,2]

Package = namedtuple('Package','x,y,dir,val')

from curses import wrapper,newwin,init_pair,color_pair
import curses
from time import sleep
        
    


class Package_set:
    binds = {}
    output = []
    input = []
    def __init__(self,board):
        board = [x.replace('\n','') for x in board]
        mlen = max(len(x) for x in board)
        self.board = [x+' '*(mlen-len(x)) for x in board]
        self.w = mlen
        self.h = len(self.board)
        self.packages = []
    def combine_packages(self):
        ret = {}
        for x,y,dir,val in self.packages:
            key = (x,y,dir)
            if key not in ret:
                ret[key] = 0
            ret[key] += val
            #print(key,val)
        self.packages[:] = []
        for key,val in ret.items():
            x,y,dir = key
            self.packages.append(Package(x,y,dir,val))
    def process_packages(self):
        newpacks = []
        for p in self.packages:
            char = self.board[p.y][p.x]
            for k,v in self.binds.items():
                if char in k:
                    arg = k.index(char)
                    #print(v.__name__,p.x,p.y,p.dir,p.val,arg)
                    res = v(p.x,p.y,p.dir,p.val,arg)
                    newpacks.extend([Package(*x) for x in res])
                    break
            else:
                newpacks.append(p)
        self.packages = newpacks
    def move_packages(self):
        self.packages = [Package(
                    (x+DIRS[dir][0]+self.w)%self.w,
                    (y+DIRS[dir][1]+self.h)%self.h,
                    dir,
                    val) 
                         for x,y,dir,val in self.packages]
        
            
        
    def create_packages(self):
        for y,row in enumerate(self.board):
            for x,c in enumerate(row):
                if c == '&':
                    for dir in range(4):
                        self.packages.append(Package(x,y,dir,0))
    def display(self):
        r = ['+','-'*self.w*3,'+']
        for y,row in enumerate(self.board):
            for ycust in (-1,0,1):
                r.append('\n|')
                for x,c in enumerate(row):
                    for xcust in (-1,0,1):
                        if any(p.x==x and p.y == y and DIRS[p.dir] == (xcust,ycust) for p in self.packages):
                            q = "<^>v"[DIRS.index((xcust,ycust))]
                            r.append(q)
                        elif xcust == 0 and ycust == 0:
                            if c != ' ':
                                r.append(c)
                            else:
                                r.append(f'.')
                        elif xcust == 0:
                            r.append(' ')
                        elif ycust == 0:
                            r.append(' ')
                        else:
                            r.append('X')
                r.append('|')
        r.extend(['\n+','-'*self.w*3,'+\n'])
        r.extend([' '.join(map(str,x)) for x in self.packages])
        r.extend([f'  {x}\n' for x in self.output])
        return ''.join(r)



class Package_display:
    def __init__(self,ps):
        self.ps = ps
        self.window = newwin(self.ps.h*2+3,self.ps.w*2+3,0,0)
        self.window2 = newwin(20,self.ps.w*2+3,self.ps.h*2+5,0)
        self.window3 = newwin(20+self.ps.h*2+3,20,0,self.ps.w*2+3)
    def init_window(self):
        self.window.addstr(0,0,'+'+'-'*self.ps.w*2+'+\n'+ ('|'+' '*self.ps.w*2+'|\n')*self.ps.h*2+'+'+'-'*self.ps.w*2+'+\n')
        for y,row in enumerate(self.ps.board):
            for x,c in enumerate(row):
                self.window.addch(2*y+2,2*x+2,c,color_pair(2))
        for x,y,dir,val in self.ps.packages:
            vx,vy = DIRS[dir]
            c = '<^>v'[dir]
            self.window.addch(2*y+2+vy,2*x+2+vx,c,color_pair(1))
        self.window2.addstr(0,0,'\n'.join(['({:02},{:02}){} {:>8.2f}'.format(p.x,p.y,'<^>v'[p.dir],float(p.val)) for p in self.ps.packages])+'\n\n')
        self.window3.addstr(0,0,''.join(self.ps.output))
    def doloop(self):
        self.ps.create_packages()
        
        while self.ps.packages:
            self.window.erase()
            self.window2.erase()
            self.window3.erase()
            self.init_window()
            self.window.refresh()
            self.window2.refresh()
            self.window3.refresh()
            #sleep(1)
            sleep(0.05)
            self.ps.move_packages()
            self.ps.process_packages()
            self.ps.combine_packages()
        self.window.getch()
                
                    
 
def bind(chars):
    def decorator(func):
        Package_set.binds[chars] = func
        return func
    return decorator
                   
@bind('<^>v')
def move(x,y,dir,val,arg):
    return (x,y,arg,val),
    
@bind('$')
def nullify(x,y,dir,val,arg):
    return (x,y,dir,0),

@bind('*')
def destr(x,y,dir,val,arg):
    return ()

@bind('0123456789')
def setval(x,y,dir,val,arg):
    return (x,y,dir,10*val+arg),

@bind('p')
def setval(x,y,dir,val,arg):
    Package_set.output.append(f'{val:.8f}'+'\n')
    return (x,y,dir,val),

@bind('P')
def setval(x,y,dir,val,arg):
    Package_set.output.append(chr(int(val+0.5)%256))
    return (x,y,dir,val),

@bind('i')
def getval(x,y,dir,val,arg):
    return (x,y,dir,float(Package_set.input.pop())),

@bind('/')
def fm(x,y,dir,val,arg):
    return (x,y,formirror[dir],val),

@bind('\\')
def bm(x,y,dir,val,arg):
    return (x,y,backmirror[dir],val),

@bind('+')
def split(x,y,dir,val,arg):
    return (x,y,(dir+1)%4,val),(x,y,(dir+3)%4,val),

@bind('#')
def split3(x,y,dir,val,arg):
    return (x,y,(dir+1)%4,val),(x,y,(dir)%4,val),(x,y,(dir+3)%4,val),

@bind('SZ')
def split(x,y,dir,val,arg):
    return (x,y,(dir+1+arg*2)%4,val)


@bind('-')
def negate(x,y,dir,val,arg):
    return (x,y,dir,-val),


@bind('e')
def expo(x,y,dir,val,arg):
    return (x,y,dir,exp(val)),

@bind('l')
def loga(x,y,dir,val,arg):
    return (x,y,dir,log(abs(val)) if val != 0 else -float('inf')),

@bind('?!')
def if_(x,y,dir,val,arg):
    if val>0 or (val==0 and arg):
        return (x,y,dir,val),
    else:
        return ()
    
@bind('o')
def delay(x,y,dir,val,arg):
    if val>0:
        vx,vy = DIRS[dir]
        return (x-vx,y-vy,dir,val-1),
    else:
        return (x,y,dir,0.0),
    
@bind('s')
def stick(x,y,dir,val,arg):
    if val<=0:
        vx,vy = DIRS[dir]
        return (x-vx,y-vy,dir,val),
    else:
        return (x,y,dir,0)


def main(stdcr):
    with open(sys.argv[1]) as f:
        p = Package_set(f)
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
    Package_display(p).doloop()
    
                
    
        
if __name__=='__main__':
    Package_set.input = [float(x) for x in sys.argv[2:]]
    wrapper(main)
    print(Package_set.output)