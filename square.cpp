
#include <iostream>
#include <queue>
#include <math.h>
struct P
{
    int delay;
    int node_parent;
    double value;

    P(int delay, int node_parent, double value) : delay(delay), node_parent(node_parent), value(value)
    {
    }

    bool operator<(const struct P& other) const
    {
        return delay > other.delay;
    }

    void display(char* txt)
    {
        //std::cout << txt << " " << delay << ' ' << node_parent << ' ' << value << std::endl;
    }
};

int main()
{
    std::priority_queue<P> pq;
    pq.push(P(0,0,0.0));
    while (!pq.empty()) {
        P topel = pq.top();
        pq.pop();
        if (!pq.empty()) {
            P nextel = pq.top();
            // if the two top packages can be combined in a single package, do that
            if (topel.delay==nextel.delay && topel.node_parent == nextel.node_parent)
            {
                // also remove second package
                pq.pop();
                // combine
                //topel.display("combine1");
                //nextel.display("combine2");
                P VL = P(topel.delay,topel.node_parent,topel.value + nextel.value);
                //VL.display("combined");
                // add again
                pq.push(VL);
                //reset so multiple packages can be combined in a loop
                continue;
            }
        }
        //topel.display("exec");

        if (topel.node_parent==0) {
            int delay = topel.delay;
            double value = topel.value;

            delay += 1;
            pq.push(P(delay,1,value));
        }

        if (topel.node_parent==1) {
            int delay = topel.delay;
            double value = topel.value;

            delay += 1;
            std::cin >> value;
            pq.push(P(delay,2,value));
        }

        if (topel.node_parent==2) {
            int delay = topel.delay;
            double value = topel.value;

            delay += 7;
            if (value >= 0) {
                pq.push(P(delay,3,value));
            }
        }

        if (topel.node_parent==2) {
            int delay = topel.delay;
            double value = topel.value;

            delay += 0;
            pq.push(P(delay,7,value));
        }

        if (topel.node_parent==3) {
            int delay = topel.delay;
            double value = topel.value;

            delay += 4;
            value = log(value);
            pq.push(P(delay,4,value));
        }

        if (topel.node_parent==3) {
            int delay = topel.delay;
            double value = topel.value;

            delay += 0;
            pq.push(P(delay,8,value));
        }

        if (topel.node_parent==4) {
            int delay = topel.delay;
            double value = topel.value;

            delay += 1;
            value = exp(value);
            pq.push(P(delay,5,value));
        }

        if (topel.node_parent==5) {
            int delay = topel.delay;
            double value = topel.value;

            delay += 1;
            std::cout << "out: " << value << std::endl;
            pq.push(P(delay,6,value));
        }

        if (topel.node_parent==7) {
            int delay = topel.delay;
            double value = topel.value;

            delay += 7;
            value = -value;
            if (value >= 0) {
                pq.push(P(delay,3,value));
            }
        }

        if (topel.node_parent==8) {
            int delay = topel.delay;
            double value = topel.value;

            delay += 4;
            value = log(value);
            pq.push(P(delay,4,value));
        }
    }
}
